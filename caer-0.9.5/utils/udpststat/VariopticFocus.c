#include <stdio.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "VariopticFocus.h"

#define MESSAGE_MAX	32

#define STX         0x02
#define LENS_WRITE  0x37
#define LENS_READ   0x38

/* Write register addresses */
#define FOCUS_LSB       0x00
#define FOCUS_MSB       0x01
#define CONTROL         0x02
#define MODE            0x03

/* Read register addresses */
#define PART_NUM        0x04
#define SW_VERSION      0x05
#define SERIAL_NUM_B0   0x06
#define SERIAL_NUM_B1   0x07
#define SERIAL_NUM_B2   0x08
#define SERIAL_NUM_B3   0x09

#define FOC_MAX			46000
#define FOC_MIN			0
#define FOC_WRITE_LEN	6

#define WRITE_FAIL		5 /* This many write tries, and we bail with error */
#define BAUD_SLEEP		20

#define ACK				0x06

static int set_interface_attribs(int fd, int speed, int parity)
{
	struct termios tty;
	memset(&tty, 0, sizeof tty);
	if (tcgetattr(fd, &tty) != 0) {
		printf("error %d from tcgetattr", errno);
		return -1;
	}

	cfsetospeed(&tty, speed);
	cfsetispeed(&tty, speed);

	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
	// disable IGNBRK for mismatched speed tests; otherwise receive break
	// as \000 chars
	tty.c_iflag &= ~IGNBRK;         // disable break processing
	tty.c_lflag = 0;                // no signaling chars, no echo,
	// no canonical processing
	tty.c_oflag = 0;                // no remapping, no delays
	tty.c_cc[VMIN]  = 0;            // read doesn't block
	tty.c_cc[VTIME] = 50;            // 0.5 seconds read timeout

	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

	tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
	// enable reading
	tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;

	if (tcsetattr(fd, TCSANOW, &tty) != 0) {
		printf("error %d from tcsetattr", errno);
		return -1;
	}

	return 0;
}

static int set_blocking(int fd, int should_block)
{
	struct termios tty;
	memset(&tty, 0, sizeof tty);
	if (tcgetattr(fd, &tty) != 0) {
		printf("error %d from tggetattr", errno);
		return -1;
	}

	tty.c_cc[VMIN] = should_block ? 1 : 0;
	tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

	if (tcsetattr(fd, TCSANOW, &tty) != 0) {
		printf("error %d setting term attributes", errno);
		return -1;
	}

	return 0;
}

static int send_foc_message(int fd, char *msg)
{
	char resp[FOC_WRITE_LEN];
	unsigned char fail = 0;
	do {
		if (write(fd, msg, FOC_WRITE_LEN) < 0) {
			printf("Error on write.\n");
			return -1;
		}

		usleep(BAUD_SLEEP * FOC_WRITE_LEN);

		if (read(fd, resp, sizeof(resp)) < 0) {
			perror("Error on read.\n");
			return -1;
		} else if (resp[2] == ACK) { 
			return 0;
		}
		printf("HICCUP\n");
	} while ((resp[2] != ACK) && (fail++ < WRITE_FAIL));

	return -1; /* Too many tries, giving up */
}

int lens_init(char *port)
{
	int fd = open(port, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0) {
		printf("Error %d opening %s: %s", errno, port, strerror(errno));
		return -1;
	}

	if (set_interface_attribs(fd, B57600, 0) < 0) {
		printf("Error on setting interface attributes.\n");
		return -1;
	}

	if (set_blocking(fd, 0) < 0) {
		printf("Error on setting blocking.\n");
		return -1;
	}

	return fd;
}

int lens_close(int fd)
{
	return close(fd);
}

int set_focus(int fd, unsigned short val)
{
	char msg[FOC_WRITE_LEN];
	msg[0] = STX; 
	msg[1] = LENS_WRITE;
	msg[3] = 0x01; /* Only writing to one register */

	/* Check the limits of the value (0 - 0xB3B0) */
	if (val > FOC_MAX) {
		val = FOC_MAX;
	}

	/* Send focus MSB register */
	msg[2] = FOCUS_MSB;
	msg[4] = (val & 0xff00) >> 8;
	msg[5] = msg[0] + msg[1] + msg[2] + msg[3] + msg[4];

	if (send_foc_message(fd, msg) < 0) {
		printf("Error sending focus packet to lens.\n");
		return -1;
	}

	/* Send focus LSB register */
	msg[2] = FOCUS_LSB;
	msg[4] = val & 0x00ff;
	msg[5] = msg[0] + msg[1] + msg[2] + msg[3] + msg[4];

	if (send_foc_message(fd, msg) < 0) {
		printf("Error sending focus packet to lens.\n");
		return -1;
	}

	return 0;
}
