/*
 * udpststat.c
 *
 *  Created on: Jan 14, 2014
 *      Author: llongi
 *	Modified by: Alireza Khodamoradi
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "VariopticFocus.h"

#define PRINTF_LOG 1
#include "events/common.h"
#include "math.h"

/* general */
CvMat *my_frame;
char key;
int delta;
void init_main(void);
void get_frame(uint32_t nano_sec);
void update_pixel(CvMat *image, uint32_t x, uint32_t y, CvScalar value);

/* for UDP client */
int listenUDPSocket;
size_t dataBufferLength;
uint8_t *dataBuffer;

/* median filter */
CvMat *filt_img;
uint32_t pixelsTS[128][128];
int delta_ts, ts_scalar, neigh;
void reset_pixels_TS(void);
bool filter_pixels_TS(int x, int y, uint32_t ts);

/* lens control */
#define LENS_PATH "/dev/ttyUSB1"
unsigned short lens_value;
int fd;

/* GHPF */
double pixelDistance(double u, double v);
double gaussianCoeff(double u, double v, double d0);
void createGaussianHighPassFilter(CvSize my_size, double cutoffInPixels);
CvMat* ghpf;

int main(void)
{
    init_main();

    int expose_t = 20;
    get_frame(expose_t*500);
    unsigned short lens_old = lens_value;

    cvNamedWindow("DVS_Median_Filtered", CV_WINDOW_NORMAL);

    cvNamedWindow("DFT Filter", CV_WINDOW_NORMAL);

    cvNamedWindow("Result", CV_WINDOW_NORMAL);

    int cut_off = 2;

    int threshold = 24;
    int is_thresh = 0;

    cvCreateTrackbar("expose(0.5mSec)", "DVS_Median_Filtered", &expose_t, 200 ,NULL);

    cvCreateTrackbar("Cut Off", "DFT Filter", &cut_off, 50 ,NULL);

    cvCreateTrackbar("Event Val*0.05", "DVS_SRC", &delta, 10 ,NULL);

    cvCreateTrackbar("En offset", "Result", &is_thresh, 1 ,NULL);
    cvCreateTrackbar("offset", "Result", &threshold, 255 ,NULL);

    cvCreateTrackbar("Lens FL", "DVS_Median_Filtered", &lens_value, 46000 ,NULL);
    cvCreateTrackbar("neighbor", "DVS_Median_Filtered", &neigh, 7 ,NULL);
    cvCreateTrackbar("Delta TS", "DVS_Median_Filtered", &delta_ts, 100 ,NULL);
    cvCreateTrackbar("TS scalar", "DVS_Median_Filtered", &ts_scalar, 3 ,NULL);

    CvMat *result;
    CvMat *complex_mat, *zero;
    int dft_m = cvGetOptimalDFTSize(128);
    int dft_n = cvGetOptimalDFTSize(128);
    printf("m=%d, n=%d\n", dft_m, dft_n);

    /* GHPF */
    CvMat* planes[2];
    planes[0] = cvCreateMat(dft_m, dft_n, CV_32FC1);
    planes[1] = cvCreateMat(dft_m, dft_n, CV_32FC1);

    complex_mat   = cvCreateMat(dft_m, dft_n, CV_32FC2);

    zero      = cvCreateMat(dft_m, dft_n, CV_32FC1);
    cvSet(zero,cvScalarAll(0.0),0);

    result = cvCreateMat(dft_m, dft_n, CV_32FC1);

    /* time */
    struct timespec start_proc, end_proc;
    double nano_sec=0;
    int counter=0;

    /* file save */
    char file_name[128];
    int img_cnt = 0;
    char number[10];
    CvMat* save_temp;
    save_temp = cvCreateMat(128, 128, CV_8U);

    while (key != 'q')
    {
        clock_gettime(CLOCK_MONOTONIC, &start_proc);

        get_frame(expose_t*1000);
        if(lens_old != lens_value)
        {
            set_focus(fd, lens_value);
            lens_old = lens_value;
        }

        cvMerge(filt_img, zero, NULL, NULL, complex_mat);

        cvDFT(complex_mat, complex_mat, CV_DXT_FORWARD, complex_mat->rows);

        createGaussianHighPassFilter(cvSize(dft_m, dft_n), cut_off);

        cvSplit(complex_mat, planes[0], planes[1], NULL, NULL);
        cvMul(planes[0], ghpf, planes[0], 1);
        cvMul(planes[1], ghpf, planes[1], 1);
        cvMerge(planes[0], planes[1], NULL, NULL, complex_mat);

        cvDFT(complex_mat, result, CV_DXT_INVERSE_SCALE, complex_mat->rows);

        if(is_thresh)
        {
            cvThreshold(result, result, threshold/255.0, 1.0, CV_THRESH_BINARY_INV);
        }

        /* file save */
        if(key=='w')
        {
            sprintf(file_name, "/home/ece191/Desktop/dump/img");
            sprintf(number, "_%d.bmp", img_cnt);
            strcat(file_name, number);
            printf("name = %s\n", file_name);
            cvConvertImage(result, save_temp, 0);
            cvSaveImage(file_name, save_temp, NULL);
            img_cnt++;
        }

        cvShowImage("DVS_Median_Filtered", filt_img);

        cvShowImage("DFT Filter", ghpf);
        cvShowImage("Result", result);
        key = cvWaitKey(1);

        clock_gettime(CLOCK_MONOTONIC, &end_proc);
        nano_sec += (double)(1000000000 * (end_proc.tv_sec - start_proc.tv_sec) + end_proc.tv_nsec - start_proc.tv_nsec);
        counter++;
        if(nano_sec>1000000000)
        {
            printf("%d per second\n", counter);
            counter=0;
            nano_sec=0;
        }
    }

    close(listenUDPSocket);
    free(dataBuffer);

    lens_close(fd);

    return (EXIT_SUCCESS);
}

void init_main(void)
{
    delta = 2;
    fd = lens_init(LENS_PATH);

    my_frame = cvCreateMat(128, 128, CV_32FC1);
    filt_img = cvCreateMat(128, 128, CV_32FC1);

    reset_pixels_TS();

    key =0;
    delta_ts = 100;
    ts_scalar = 2;
    neigh=3;

    lens_value = 23500;
    set_focus(fd, lens_value);

    // First of all, parse the IP:Port we need to listen on.
    // Those are for now also the only two parameters permitted.
    // If none passed, attempt to connect to default UDP IP:Port.
    const char *ipAddress = "127.0.0.1";
    uint16_t portNumber = 8888;

    // Create listening socket for UDP data.
    listenUDPSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (listenUDPSocket < 0)
    {
        printf("Failed to create UDP socket.\n");
    }

    struct sockaddr_in listenUDPAddress;
    memset(&listenUDPAddress, 0, sizeof(struct sockaddr_in));

    listenUDPAddress.sin_family = AF_INET;
    listenUDPAddress.sin_port = htons(portNumber);
    inet_aton(ipAddress, &listenUDPAddress.sin_addr); // htonl() is implicit here.

    if (bind(listenUDPSocket, (struct sockaddr *) &listenUDPAddress, sizeof(struct sockaddr_in)) < 0)
    {
        printf("Failed to listen on UDP socket.\n");
    }
    // 64K data buffer should be enough for the UDP packets.
    dataBufferLength = 1024 * 64;
    dataBuffer = malloc(dataBufferLength);
}

void reset_pixels_TS(void)
{
        for(int x=0; x<128; x++)
        {
            for(int y=0; y<128; y++)
            {
                pixelsTS[x][y]=0;
            }
        }
}

void get_frame(uint32_t nano_sec)
{
    cvSet(my_frame,cvScalarAll(0.5),0);
    cvSet(filt_img,cvScalarAll(0.5),0);

    uint32_t i;//just for loop - Ali
    uint32_t j,x_axis,y_axis;//just for the print loop - Ali
    CvScalar l_value = cvScalar(-0.05*delta, 0, 0, 0);
    CvScalar h_value = cvScalar(0.05*delta, 0, 0, 0);
    CvScalar p_value;
    uint32_t nSec=0;

    while (nSec < nano_sec)
    {
        //clock_gettime(CLOCK_MONOTONIC, &start);
        ssize_t result = recv(listenUDPSocket, dataBuffer, dataBufferLength, 0);
        if (result <= 0)
        {
            printf("Error in recv() call: %d\n", errno);
            //break;
        }

        caerEventPacketHeader header = (caerEventPacketHeader) dataBuffer;

        uint32_t eventValid = caerEventPacketHeaderGetEventValid(header);

		if (eventValid > 0) {
			void *firstEvent = caerGenericEventGetEvent(header, 0);
			void *lastEvent = caerGenericEventGetEvent(header, eventValid - 1);

			uint32_t firstTS = caerGenericEventGetTimestamp(firstEvent, header);
			uint32_t lastTS = caerGenericEventGetTimestamp(lastEvent, header);

			nSec += lastTS - firstTS;
		}

        for(i=0; i<eventValid; i++) //loop - Ali
        {
            void *currentEvent = caerGenericEventGetEvent(header, i);
            j = (le32toh(*((uint32_t *) (((uint8_t *) currentEvent)))));
            j = j/2;//validity bit
            if(j&1)
            {
                p_value = h_value;
            }
            else
            {
                p_value = l_value;
            }
            j = j/32;// polarity bit and 4 NU bits total of 2^5
            x_axis = j&127;// 7 bits for x address
            j = j/8192;// 7 bits for x and 6 NU bits = 2^13 = 8192
            y_axis = j&127;// 7 bits for y axis

            uint32_t currentTS = caerGenericEventGetTimestamp(currentEvent, header);

            if(filter_pixels_TS(127-(int)x_axis, 127-(int)y_axis, currentTS))
            {
                update_pixel(filt_img, x_axis, y_axis, p_value);// cvSet2D(filt_img, 127-(int)x_axis, 127-(int)y_axis, p_value+cvScalar(0.5, 0, 0, 0));
            }
            update_pixel(my_frame, x_axis, y_axis, p_value);

            pixelsTS[127-(int)x_axis][127-(int)y_axis] = currentTS;
        }
    }
    usleep(1);
}

void update_pixel(CvMat *image, uint32_t x, uint32_t y, CvScalar value)
{
    CvScalar temp = cvGet2D(image, 127-(int)x, 127-(int)y);
    temp.val[0] += value.val[0];

    cvSet2D(image, 127-(int)x, 127-(int)y, temp);
}


bool filter_pixels_TS(int x, int y, uint32_t ts)
{
    for(int i=x-neigh; i<x+neigh+1; i++) // search in a 3x3 neighborhood
    {
        for(int j=y-neigh; j<y+neigh+1; j++)
        {
            if( i>-1 && i<128 && j>-1 && j<128 ) //boundary for 0 <= x and y <= 127
            {
                if( !(i==x && j==y) )
                {
                    if( (ts - pixelsTS[i][j]) < delta_ts*((int) pow((double) 10,ts_scalar)) && pixelsTS[i][j]!=0x80000000 ) //
                        return true;
                }
            }
        }
    }
    return false;
}

double pixelDistance(double u, double v)
{
    return sqrt(u*u + v*v);
}

double gaussianCoeff(double u, double v, double d0)
{
    double d = pixelDistance(u, v);
    return 1.0 - exp((-d*d) / (2*d0*d0));
}

void createGaussianHighPassFilter(CvSize my_size, double cutoffInPixels)
{
    ghpf = cvCreateMat(my_size.width, my_size.height, CV_32FC1);

    CvPoint center;
    center = cvPoint(my_size.width / 2, my_size.height / 2);

    int u, v;

    for(u = 0; u < my_size.height; u++)
    {
        for(v = 0; v < my_size.width; v++)
        {
            cvSet2D(ghpf, u, v, cvScalar(gaussianCoeff(u - center.y, v - center.x, cutoffInPixels),0,0,0));
        }
    }

    int cx = my_size.width/2;
    int cy = my_size.height/2;

    CvMat *q0, *q1, *q2, *q3;
    q0 = cvCreateMat(cx, cy, CV_32FC1);
    q1 = cvCreateMat(cx, cy, CV_32FC1);
    q2 = cvCreateMat(cx, cy, CV_32FC1);
    q3 = cvCreateMat(cx, cy, CV_32FC1);

    cvGetSubRect(ghpf, q0, cvRect(0, 0, cx, cy));
    cvGetSubRect(ghpf, q1, cvRect(cx, 0, cx, cy));
    cvGetSubRect(ghpf, q2, cvRect(0, cy, cx, cy));
    cvGetSubRect(ghpf, q3, cvRect(cx, cy, cx, cy));

    CvMat *tmp;                           // swap quadrants (Top-Left with Bottom-Right)
    tmp = cvCreateMat(cx, cy, CV_32FC1);
    cvCopy(q0, tmp, NULL);
    cvCopy(q3, q0, NULL);
    cvCopy(tmp, q3, NULL);

    cvCopy(q1, tmp, NULL);                  // swap quadrant (Top-Right with Bottom-Left)
    cvCopy(q2, q1, NULL);
    cvCopy(tmp, q2, NULL);

}

