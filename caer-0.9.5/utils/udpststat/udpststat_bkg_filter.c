/*
 * udpststat.c
 *
 *  Created on: Jan 14, 2014
 *      Author: llongi
 *	Modified by: Alireza Khodamoradi
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
//Ali
#include <pthread.h>
#include <time.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "VariopticFocus.h"

#define PRINTF_LOG 1
#define LENS_MAX 23000
#define LENS_MIN 16000
#define LENS_STEP 1000

#include "events/common.h"

CvMat *my_frame, *filt_img;

int listenUDPSocket;
size_t dataBufferLength;
uint8_t *dataBuffer;

uint32_t pixelsTS[128][128];

//int threshold1, threshold2;
//int aperture_size;

void* lens_thread(void* param);
unsigned short get_lens_value(unsigned short current_lens_value);
unsigned short lens_value;
int fd, l_m;
char key;

int delta_ts, ts_scalar, neigh;

void reset_pixels(void);
bool filter_pixels(int x, int y, uint32_t ts);

void init_main(void);
void get_frame(uint32_t nano_sec);

int main(void)
{
    init_main();

    fd = lens_init("/dev/ttyUSB0");
    pthread_t lens_t;
    //pthread_create(&lens_t, NULL, lens_thread, NULL);

    key =0;

    int expose = 14;
    delta_ts = 100;
    ts_scalar = 2;
    neigh=3;

    get_frame(expose*1000);

    cvNamedWindow("DVS_SRC", CV_WINDOW_NORMAL);
    cvNamedWindow("DVS_Filtered", CV_WINDOW_NORMAL);
    cvCreateTrackbar("Expose time ms", "DVS_SRC", &expose, 1000 ,NULL);
    cvCreateTrackbar("neighbor", "DVS_SRC", &neigh, 7 ,NULL);
    cvCreateTrackbar("Delta TS", "DVS_Filtered", &delta_ts, 100 ,NULL);
    cvCreateTrackbar("TS scalar", "DVS_Filtered", &ts_scalar, 3 ,NULL);

    while (key != 'q')
    {
        get_frame(expose*1000);
        // Ali's time median
        cvShowImage("DVS_Filtered", filt_img);
        cvShowImage("DVS_SRC", my_frame);
        key = cvWaitKey(1);

        if(key=='w')
            sleep(1);

    }

    close(listenUDPSocket);
    free(dataBuffer);

    lens_close(fd);

    return (EXIT_SUCCESS);
}

void init_main(void)
{
    reset_pixels();
    l_m = 1;
    lens_value = LENS_MIN;
    my_frame = cvCreateMat(128, 128, CV_8UC1);
    filt_img = cvCreateMat(128, 128, CV_8UC1);

    // First of all, parse the IP:Port we need to listen on.
    // Those are for now also the only two parameters permitted.
    // If none passed, attempt to connect to default UDP IP:Port.
    const char *ipAddress = "127.0.0.1";
    uint16_t portNumber = 8888;

    // Create listening socket for UDP data.
    listenUDPSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (listenUDPSocket < 0)
    {
        printf("Failed to create UDP socket.\n");
    }

    struct sockaddr_in listenUDPAddress;
    memset(&listenUDPAddress, 0, sizeof(struct sockaddr_in));

    listenUDPAddress.sin_family = AF_INET;
    listenUDPAddress.sin_port = htons(portNumber);
    inet_aton(ipAddress, &listenUDPAddress.sin_addr); // htonl() is implicit here.

    if (bind(listenUDPSocket, (struct sockaddr *) &listenUDPAddress, sizeof(struct sockaddr_in)) < 0)
    {
        printf("Failed to listen on UDP socket.\n");
    }
    // 64K data buffer should be enough for the UDP packets.
    dataBufferLength = 1024 * 64;
    dataBuffer = malloc(dataBufferLength);
}

void reset_pixels(void)
{
        for(int x=0; x<128; x++)
        {
            for(int y=0; y<128; y++)
            {
                pixelsTS[x][y]=0;
            }
        }

}

void get_frame(uint32_t nano_sec)
{
    cvSet(my_frame,cvScalarAll(128),0);
    cvSet(filt_img,cvScalarAll(128),0);
    int i;//just for loop - Ali
    uint32_t j,x_axis,y_axis;//just for the print loop - Ali
    CvScalar l_value = cvScalar(0, 0, 0, 0);
    CvScalar h_value = cvScalar(255, 0, 0, 0);
    CvScalar p_value;
    uint32_t nSec=0;
    while (nSec < nano_sec)
    {
        //clock_gettime(CLOCK_MONOTONIC, &start);
        ssize_t result = recv(listenUDPSocket, dataBuffer, dataBufferLength, 0);
        if (result <= 0)
        {
            printf("Error in recv() call: %d\n", errno);
            //break;
        }

        caerEventPacketHeader header = (caerEventPacketHeader) dataBuffer;

        uint32_t eventValid = caerEventPacketHeaderGetEventValid(header);

		if (eventValid > 0) {
			void *firstEvent = caerGenericEventGetEvent(header, 0);
			void *lastEvent = caerGenericEventGetEvent(header, eventValid - 1);

			uint32_t firstTS = caerGenericEventGetTimestamp(firstEvent, header);
			uint32_t lastTS = caerGenericEventGetTimestamp(lastEvent, header);

			nSec += lastTS - firstTS;
		}

        for(i=0; i<eventValid; i++) //loop - Ali
        {
            void *currentEvent = caerGenericEventGetEvent(header, i);
            j = (le32toh(*((uint32_t *) (((uint8_t *) currentEvent)))));
            j = j/2;//validity bit
            if(j&1)
            {
                p_value = h_value;
            }
            else
            {
                p_value = l_value;
            }
            j = j/32;// polarity bit and 4 NU bits total of 2^5
            x_axis = j&127;// 7 bits for x address
            j = j/8192;// 7 bits for x and 6 NU bits = 2^13 = 8192
            y_axis = j&127;// 7 bits for y axis

            uint32_t currentTS = caerGenericEventGetTimestamp(currentEvent, header);

            if(filter_pixels(127-(int)x_axis, 127-(int)y_axis, currentTS))
            {
                cvSet2D(filt_img, 127-(int)x_axis, 127-(int)y_axis, p_value);
            }
            pixelsTS[127-(int)x_axis][127-(int)y_axis] = currentTS;
            cvSet2D(my_frame, 127-(int)x_axis, 127-(int)y_axis, p_value);
        }

    }
}

bool filter_pixels(int x, int y, uint32_t ts)
{
    for(int i=x-neigh; i<x+neigh+1; i++) // search in a 3x3 neighborhood
    {
        for(int j=y-neigh; j<y+neigh+1; j++)
        {
            if( i>-1 && i<128 && j>-1 && j<128 ) //boundary for 0 <= x and y <= 127
            {
                if( !(i==x && j==y) )
                {
                    if( (ts - pixelsTS[i][j]) < delta_ts*((int) pow((double) 10,ts_scalar)) && pixelsTS[i][j]!=0x80000000 ) //
                        return true;
                }
            }
        }
    }
    return false;
}

void* lens_thread(void* param)
{
    while(key != 'q')
    {
        usleep(1000);
        set_focus(fd, lens_value);
        lens_value = get_lens_value(lens_value);
    }
}

unsigned short get_lens_value(unsigned short current_lens_value)
{
    unsigned short new_lens_val = current_lens_value + l_m*LENS_STEP;

    if(new_lens_val==LENS_MAX)
    {
        l_m = -1;
    }

    if(new_lens_val==LENS_MIN)
    {
        l_m = 1;
    }
    return new_lens_val;
}
