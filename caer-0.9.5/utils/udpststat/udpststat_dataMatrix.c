/*
 * udpststat.c
 *
 *  Created on: Jan 14, 2014
 *      Author: llongi
 *	Modified by: Alireza Khodamoradi
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "VariopticFocus.h"

#define PRINTF_LOG 1
#include "events/common.h"
#include "math.h"

/* general */
CvMat *my_frame;
char key;
int delta;
void init_main(void);
void get_frame(uint32_t nano_sec);
void update_pixel(CvMat *image, uint32_t x, uint32_t y, CvScalar value);

/* for UDP client */
int listenUDPSocket;
size_t dataBufferLength;
uint8_t *dataBuffer;

/* median filter */
CvMat *filt_img;
uint32_t pixelsTS[128][128];
int delta_ts, ts_scalar, neigh;
void reset_pixels_TS(void);
bool filter_pixels_TS(int x, int y, uint32_t ts);

/* lens control */
#define LENS_MAX 24000
#define LENS_MIN 2000
#define LENS_STEP 1000
#define LENS_PATH "/dev/ttyUSB0"
void* lens_thread(void* param);
unsigned short get_lens_value(unsigned short current_lens_value);
unsigned short lens_value;
int fd, l_m;

int main(void)
{
    init_main();
/*
    pthread_t lens_t;
    pthread_create(&lens_t, NULL, lens_thread, NULL);
*/
    CvMat *img_resize;
    img_resize = cvCreateMat(512, 512, CV_8UC1);
    int expose_t = 20;
    get_frame(expose_t*500);
    cvResize(filt_img, img_resize, CV_INTER_LINEAR);

    /* edge */
    int lowThresh = 20;
	int highThresh = 40;
    int N=5;
    IplImage stub, *picture;
    CvMat *temp_img;
    temp_img = cvCloneMat(img_resize);
    IplImage* img;
    img = cvGetImage(temp_img, &stub);
    //IplImage* img_b = cvCreateImage( cvSize(img->width+N-1,img->height+N-1), img->depth, img->nChannels );
    IplImage* out = cvCreateImage( cvGetSize(img), IPL_DEPTH_8U, img->nChannels );
    cvNamedWindow("canny", CV_WINDOW_NORMAL);
    /* **** */

    cvNamedWindow("DVS_Median_Filtered", CV_WINDOW_NORMAL);

    int edge_th1=100, edge_th2=200, aperture_size=3;

    unsigned short lens_old = lens_value;
    cvCreateTrackbar("lens", "DVS_Median_Filtered", &lens_value, 46000 ,NULL);
    cvCreateTrackbar("expose(0.5mSec)", "DVS_Median_Filtered", &expose_t, 100 ,NULL);
    cvCreateTrackbar("delta", "DVS_Median_Filtered", &delta, 100 ,NULL);
    cvCreateTrackbar("T 1", "DVS_Median_Filtered", &lowThresh, 100 ,NULL);
    cvCreateTrackbar("T 2", "DVS_Median_Filtered", &highThresh, 100 ,NULL);
    char file_name[128];
    int img_cnt = 0;
    char number[10];

    while (key != 'q')
    {
        get_frame(expose_t*1000);
        cvResize(filt_img, img_resize, CV_INTER_LINEAR);
        if(lens_old != lens_value)
        {
            set_focus(fd, lens_value);
            lens_old = lens_value;
        }

        /* edge */
        temp_img = cvCloneMat(img_resize);
        img = cvGetImage(temp_img, &stub);
        cvCanny( img, out, (double)lowThresh*N*N, (double)highThresh*N*N, N );
        cvShowImage("canny", out);
        /* **** */

        if(key=='w')
        {
            sprintf(file_name, "/home/ece191/Desktop/dump/img");
            sprintf(number, "_%d.bmp", img_cnt);
            strcat(file_name, number);
            printf("name = %s\n", file_name);
            cvSaveImage(file_name, out, 0);
            img_cnt++;
        }

        cvShowImage("DVS_Median_Filtered", filt_img);

        key = cvWaitKey(1);

    }

    close(listenUDPSocket);
    free(dataBuffer);

    lens_close(fd);

    return (EXIT_SUCCESS);
}

void init_main(void)
{
    delta = 25;
    fd = lens_init("/dev/ttyUSB0");
    lens_value = 23000;//LENS_MIN;

    my_frame = cvCreateMat(128, 128, CV_8UC1);
    filt_img = cvCreateMat(128, 128, CV_8UC1);

    reset_pixels_TS();

    key =0;
    delta_ts = 100;
    ts_scalar = 2;
    neigh=3;

    // First of all, parse the IP:Port we need to listen on.
    // Those are for now also the only two parameters permitted.
    // If none passed, attempt to connect to default UDP IP:Port.
    const char *ipAddress = "127.0.0.1";
    uint16_t portNumber = 8888;

    // Create listening socket for UDP data.
    listenUDPSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (listenUDPSocket < 0)
    {
        printf("Failed to create UDP socket.\n");
    }

    struct sockaddr_in listenUDPAddress;
    memset(&listenUDPAddress, 0, sizeof(struct sockaddr_in));

    listenUDPAddress.sin_family = AF_INET;
    listenUDPAddress.sin_port = htons(portNumber);
    inet_aton(ipAddress, &listenUDPAddress.sin_addr); // htonl() is implicit here.

    if (bind(listenUDPSocket, (struct sockaddr *) &listenUDPAddress, sizeof(struct sockaddr_in)) < 0)
    {
        printf("Failed to listen on UDP socket.\n");
    }
    // 64K data buffer should be enough for the UDP packets.
    dataBufferLength = 1024 * 64;
    dataBuffer = malloc(dataBufferLength);
}

void reset_pixels_TS(void)
{
        for(int x=0; x<128; x++)
        {
            for(int y=0; y<128; y++)
            {
                pixelsTS[x][y]=0;
            }
        }
}

void get_frame(uint32_t nano_sec)
{
    cvSet(my_frame,cvScalarAll(128),0);
    cvSet(filt_img,cvScalarAll(128),0);

    uint32_t i;//just for loop - Ali
    uint32_t j,x_axis,y_axis;//just for the print loop - Ali
    CvScalar l_value = cvScalar(-1*delta, 0, 0, 0);
    CvScalar h_value = cvScalar(1*delta, 0, 0, 0);
    CvScalar p_value;
    uint32_t nSec=0;

    while (nSec < nano_sec)
    {
        //clock_gettime(CLOCK_MONOTONIC, &start);
        ssize_t result = recv(listenUDPSocket, dataBuffer, dataBufferLength, 0);
        if (result <= 0)
        {
            printf("Error in recv() call: %d\n", errno);
            //break;
        }

        caerEventPacketHeader header = (caerEventPacketHeader) dataBuffer;

        uint32_t eventValid = caerEventPacketHeaderGetEventValid(header);

		if (eventValid > 0) {
			void *firstEvent = caerGenericEventGetEvent(header, 0);
			void *lastEvent = caerGenericEventGetEvent(header, eventValid - 1);

			uint32_t firstTS = caerGenericEventGetTimestamp(firstEvent, header);
			uint32_t lastTS = caerGenericEventGetTimestamp(lastEvent, header);

			nSec += lastTS - firstTS;
		}

        for(i=0; i<eventValid; i++) //loop - Ali
        {
            void *currentEvent = caerGenericEventGetEvent(header, i);
            j = (le32toh(*((uint32_t *) (((uint8_t *) currentEvent)))));
            j = j/2;//validity bit
            if(j&1)
            {
                p_value = h_value;
            }
            else
            {
                p_value = l_value;
            }
            j = j/32;// polarity bit and 4 NU bits total of 2^5
            x_axis = j&127;// 7 bits for x address
            j = j/8192;// 7 bits for x and 6 NU bits = 2^13 = 8192
            y_axis = j&127;// 7 bits for y axis

            uint32_t currentTS = caerGenericEventGetTimestamp(currentEvent, header);

            if(filter_pixels_TS(127-(int)x_axis, 127-(int)y_axis, currentTS))
            {
                update_pixel(filt_img, x_axis, y_axis, p_value);// cvSet2D(filt_img, 127-(int)x_axis, 127-(int)y_axis, p_value+cvScalar(0.5, 0, 0, 0));
            }
            update_pixel(my_frame, x_axis, y_axis, p_value);

            pixelsTS[127-(int)x_axis][127-(int)y_axis] = currentTS;
        }
    }
}

void update_pixel(CvMat *image, uint32_t x, uint32_t y, CvScalar value)
{
    CvScalar temp = cvGet2D(image, 127-(int)x, 127-(int)y);
    temp.val[0] += value.val[0];

    cvSet2D(image, 127-(int)x, 127-(int)y, temp);
}


bool filter_pixels_TS(int x, int y, uint32_t ts)
{
    for(int i=x-neigh; i<x+neigh+1; i++) // search in a 3x3 neighborhood
    {
        for(int j=y-neigh; j<y+neigh+1; j++)
        {
            if( i>-1 && i<128 && j>-1 && j<128 ) //boundary for 0 <= x and y <= 127
            {
                if( !(i==x && j==y) )
                {
                    if( (ts - pixelsTS[i][j]) < delta_ts*((int) pow((double) 10,ts_scalar)) && pixelsTS[i][j]!=0x80000000 ) //
                        return true;
                }
            }
        }
    }
    return false;
}

void* lens_thread(void* param)
{
    while(key != 'q')
    {
            set_focus(fd, lens_value);
            lens_value = get_lens_value(lens_value);
            usleep(1000);
    }
}

unsigned short get_lens_value(unsigned short current_lens_value)
{
    unsigned short new_lens_val = current_lens_value + l_m*LENS_STEP;

    if(new_lens_val==LENS_MAX)
    {
        l_m = -1;
    }

    if(new_lens_val==LENS_MIN)
    {
        l_m = 1;
    }
    return new_lens_val;

}
