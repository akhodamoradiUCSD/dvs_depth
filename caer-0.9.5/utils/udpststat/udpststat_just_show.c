/*
 * udpststat.c
 *
 *  Created on: Jan 14, 2014
 *      Author: llongi
 *      Modified by: Alireza
 *	Modified by: Alireza Khodamoradi
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "VariopticFocus.h"

#define PRINTF_LOG 1
#include "events/common.h"

/* general */
CvMat *my_frame;
char key;
int delta;
void init_main(void);
void get_frame(uint32_t nano_sec);
void update_pixel(CvMat *image, uint32_t x, uint32_t y, CvScalar value);

/* for UDP client */
int listenUDPSocket;
size_t dataBufferLength;
uint8_t *dataBuffer;

int main(void)
{
    init_main();

    int expose_t = 6;
    get_frame(expose_t*500);

    cvNamedWindow("DVS SRC", CV_WINDOW_NORMAL);

    cvCreateTrackbar("expose(0.5mSec)", "DVS SRC", &expose_t, 200 ,NULL);
    cvCreateTrackbar("Pix pol effect", "DVS SRC", &delta, 10 ,NULL);

    while (key != 'q')
    {
        get_frame(expose_t*1000);
        cvShowImage("DVS SRC", my_frame);
        key = cvWaitKey(1);

    }

    close(listenUDPSocket);
    free(dataBuffer);

    return (EXIT_SUCCESS);
}

void init_main(void)
{
    delta = 8;

    my_frame = cvCreateMat(128, 128, CV_8UC1);

    // First of all, parse the IP:Port we need to listen on.
    // Those are for now also the only two parameters permitted.
    // If none passed, attempt to connect to default UDP IP:Port.
    const char *ipAddress = "127.0.0.1";
    uint16_t portNumber = 8888;

    // Create listening socket for UDP data.
    listenUDPSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (listenUDPSocket < 0)
    {
        printf("Failed to create UDP socket.\n");
    }

    struct sockaddr_in listenUDPAddress;
    memset(&listenUDPAddress, 0, sizeof(struct sockaddr_in));

    listenUDPAddress.sin_family = AF_INET;
    listenUDPAddress.sin_port = htons(portNumber);
    inet_aton(ipAddress, &listenUDPAddress.sin_addr); // htonl() is implicit here.

    if (bind(listenUDPSocket, (struct sockaddr *) &listenUDPAddress, sizeof(struct sockaddr_in)) < 0)
    {
        printf("Failed to listen on UDP socket.\n");
    }
    // 64K data buffer should be enough for the UDP packets.
    dataBufferLength = 1024 * 64;
    dataBuffer = malloc(dataBufferLength);
}

void get_frame(uint32_t nano_sec)
{
    cvSet(my_frame,cvScalarAll(128),0);

    uint32_t i;//just for loop - Ali
    uint32_t j,x_axis,y_axis;//just for the print loop - Ali
    CvScalar l_value = cvScalar(delta, 0, 0, 0);
    CvScalar h_value = cvScalar(-delta, 0, 0, 0);
    CvScalar p_value;
    uint32_t nSec=0;

    while (nSec < nano_sec)
    {
        //clock_gettime(CLOCK_MONOTONIC, &start);
        ssize_t result = recv(listenUDPSocket, dataBuffer, dataBufferLength, 0);
        if (result <= 0)
        {
            printf("Error in recv() call: %d\n", errno);
            //break;
        }

        caerEventPacketHeader header = (caerEventPacketHeader) dataBuffer;

        uint32_t eventValid = caerEventPacketHeaderGetEventValid(header);

		if (eventValid > 0) {
			void *firstEvent = caerGenericEventGetEvent(header, 0);
			void *lastEvent = caerGenericEventGetEvent(header, eventValid - 1);

			uint32_t firstTS = caerGenericEventGetTimestamp(firstEvent, header);
			uint32_t lastTS = caerGenericEventGetTimestamp(lastEvent, header);

			nSec += lastTS - firstTS;
		}

        for(i=0; i<eventValid; i++) //loop - Ali
        {
            void *currentEvent = caerGenericEventGetEvent(header, i);
            j = (le32toh(*((uint32_t *) (((uint8_t *) currentEvent)))));
            j = j/2;//validity bit
            if(j&1)
            {
                p_value = h_value;
            }
            else
            {
                p_value = l_value;
            }
            j = j/32;// polarity bit and 4 NU bits total of 2^5
            x_axis = j&127;// 7 bits for x address
            j = j/8192;// 7 bits for x and 6 NU bits = 2^13 = 8192
            y_axis = j&127;// 7 bits for y axis

            uint32_t currentTS = caerGenericEventGetTimestamp(currentEvent, header);

            update_pixel(my_frame, x_axis, y_axis, p_value);

        }
    }
}

void update_pixel(CvMat *image, uint32_t x, uint32_t y, CvScalar value)
{
    CvScalar temp = cvGet2D(image, 127-(int)x, 127-(int)y);
    temp.val[0] += value.val[0];

    cvSet2D(image, 127-(int)x, 127-(int)y, temp);
}
