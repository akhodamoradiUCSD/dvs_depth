/*
 * udpststat.c
 *
 *  Created on: Jan 14, 2014
 *      Author: llongi
 *	Modified by: Alireza Khodamoradi
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "VariopticFocus.h"

#define PRINTF_LOG 1
#include "events/common.h"
#include "math.h"

/* general */
CvMat *my_frame;
char key;
int delta;
void init_main(void);
void get_frame(uint32_t nano_sec);
void update_pixel(CvMat *image, uint32_t x, uint32_t y, CvScalar value);

/* thread control */
bool start;

/* for UDP client */
int listenUDPSocket;
size_t dataBufferLength;
uint8_t *dataBuffer;

/* median filter */
CvMat *filt_img;
uint32_t pixelsTS[128][128];
int delta_ts, ts_scalar, neigh;
void reset_pixels_TS(void);
bool filter_pixels_TS(int x, int y, uint32_t ts);

/* lens control */
#define LENS_MAX 30000
#define LENS_MIN 10000
#define LENS_STEP 1000
#define LENS_PATH "/dev/ttyUSB1"
void* lens_thread(void* param);
unsigned short get_lens_value(unsigned short current_lens_value);
unsigned short lens_value;
int fd, l_m;

/* GHPF */
double pixelDistance(double u, double v);
double gaussianCoeff(double u, double v, double d0);
void createGaussianHighPassFilter(CvSize my_size, double cutoffInPixels);
CvMat* ghpf;

int main(void)
{
    init_main();

    pthread_t lens_t;
    pthread_create(&lens_t, NULL, lens_thread, NULL);

    get_frame(1000);//

    cvNamedWindow("DVS_SRC", CV_WINDOW_NORMAL);
    cvNamedWindow("DVS_Median_Filtered", CV_WINDOW_NORMAL);

    cvNamedWindow("DFT Filter", CV_WINDOW_NORMAL);

    cvNamedWindow("Result", CV_WINDOW_NORMAL);

    cvNamedWindow("BGR", CV_WINDOW_NORMAL);

    int threshold1 = 35;
    int cut_off = 20;

    cvCreateTrackbar("offset", "Result", &threshold1, 255 ,NULL);

    cvCreateTrackbar("Cut Off", "DFT Filter", &cut_off, 100 ,NULL);

    cvCreateTrackbar("Event Val*0.05", "DVS_SRC", &delta, 10 ,NULL);

    cvCreateTrackbar("neighbor", "DVS_Median_Filtered", &neigh, 7 ,NULL);
    cvCreateTrackbar("Delta TS", "DVS_Median_Filtered", &delta_ts, 100 ,NULL);
    cvCreateTrackbar("TS scalar", "DVS_Median_Filtered", &ts_scalar, 3 ,NULL);

    CvMat *result;
    CvMat *complex_mat, *zero, *my_ones;
    int dft_m = cvGetOptimalDFTSize(128);
    int dft_n = cvGetOptimalDFTSize(128);
    printf("m=%d, n=%d\n", dft_m, dft_n);

    /* GHPF */
    CvMat* planes[2];
    planes[0] = cvCreateMat(dft_m, dft_n, CV_32FC1);
    planes[1] = cvCreateMat(dft_m, dft_n, CV_32FC1);

    CvMat* BGR_array[3];
    BGR_array[0] = cvCreateMat(dft_m, dft_n, CV_32FC1);
    BGR_array[1] = cvCreateMat(dft_m, dft_n, CV_32FC1);
    BGR_array[2] = cvCreateMat(dft_m, dft_n, CV_32FC1);

    CvMat* my_BGR;
    my_BGR = cvCreateMat(dft_m, dft_n, CV_32FC3);

    CvMat* BGR_accu;
    BGR_accu = cvCreateMat(dft_m, dft_n, CV_32FC3);

    complex_mat   = cvCreateMat(dft_m, dft_n, CV_32FC2);

    zero = cvCreateMat(dft_m, dft_n, CV_32FC1);
    cvSet(zero,cvScalarAll(0.0),0);

    my_ones = cvCreateMat(dft_m, dft_n, CV_32FC1);
    cvSet(my_ones, cvScalarAll(1.0),0);

    result = cvCreateMat(dft_m, dft_n, CV_32FC1);

    /* time */
    struct timespec start_proc, end_proc;
    double nano_sec=0;
    int counter=0;

    double scale;
    CvScalar scale_one = cvScalar(1, 1, 1, 0);

    while (key != 'q')
    {
        clock_gettime(CLOCK_MONOTONIC, &start_proc);

        get_frame(1000);//frame is controlled by start

        cvMerge(filt_img, zero, NULL, NULL, complex_mat);

        cvDFT(complex_mat, complex_mat, CV_DXT_FORWARD, complex_mat->rows);

        createGaussianHighPassFilter(cvSize(dft_m, dft_n), cut_off);

        cvSplit(complex_mat, planes[0], planes[1], NULL, NULL);
        cvMul(planes[0], ghpf, planes[0], 1);
        cvMul(planes[1], ghpf, planes[1], 1);
        cvMerge(planes[0], planes[1], NULL, NULL, complex_mat);

        cvDFT(complex_mat, result, CV_DXT_INVERSE_SCALE, complex_mat->rows);

        if( (lens_value != LENS_MAX) & (lens_value != LENS_MIN) )
        {
            cvThreshold(result, result, threshold1/255.0, 1.0, CV_THRESH_BINARY);

            if(l_m == -1)
            {
                scale = 1 - (double)(lens_value-LENS_MIN)/(LENS_MAX-LENS_MIN);
            }
            else
            {
                scale = (double)(LENS_MAX-lens_value)/(LENS_MAX-LENS_MIN);
            }

            //printf("scale = %lf\n", scale);

            cvMul(result, my_ones, BGR_array[0], scale*scale);
            cvMul(result, my_ones, BGR_array[1], scale);
            cvMul(result, my_ones, BGR_array[2], 1);
            cvMerge(BGR_array[0], BGR_array[1], BGR_array[2], NULL, my_BGR);
            cvScaleAdd(my_BGR, scale_one, BGR_accu, BGR_accu);

            cvShowImage("BGR", BGR_accu);

        }

        else
        {
            cvSet(BGR_accu,cvScalarAll(0),0);
        }

        cvShowImage("DVS_SRC", my_frame);
        cvShowImage("DVS_Median_Filtered", filt_img);

        cvShowImage("DFT Filter", ghpf);
        cvShowImage("Result", result);
        key = cvWaitKey(1);

        clock_gettime(CLOCK_MONOTONIC, &end_proc);
        nano_sec += (double)(1000000000 * (end_proc.tv_sec - start_proc.tv_sec) + end_proc.tv_nsec - start_proc.tv_nsec);
        counter++;
        if(nano_sec>1000000000)
        {
            printf("%d per second\n", counter);
            counter=0;
            nano_sec=0;
        }
    }

    close(listenUDPSocket);
    free(dataBuffer);

    lens_close(fd);

    return (EXIT_SUCCESS);
}

void init_main(void)
{
    delta = 2;
    start = false;
    fd = lens_init(LENS_PATH);

    my_frame = cvCreateMat(128, 128, CV_32FC1);
    filt_img = cvCreateMat(128, 128, CV_32FC1);

    reset_pixels_TS();
    l_m = 1;
    lens_value = LENS_MIN;

    key =0;
    delta_ts = 100;
    ts_scalar = 2;
    neigh=3;

    // First of all, parse the IP:Port we need to listen on.
    // Those are for now also the only two parameters permitted.
    // If none passed, attempt to connect to default UDP IP:Port.
    const char *ipAddress = "127.0.0.1";
    uint16_t portNumber = 8888;

    // Create listening socket for UDP data.
    listenUDPSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (listenUDPSocket < 0)
    {
        printf("Failed to create UDP socket.\n");
    }

    struct sockaddr_in listenUDPAddress;
    memset(&listenUDPAddress, 0, sizeof(struct sockaddr_in));

    listenUDPAddress.sin_family = AF_INET;
    listenUDPAddress.sin_port = htons(portNumber);
    inet_aton(ipAddress, &listenUDPAddress.sin_addr); // htonl() is implicit here.

    if (bind(listenUDPSocket, (struct sockaddr *) &listenUDPAddress, sizeof(struct sockaddr_in)) < 0)
    {
        printf("Failed to listen on UDP socket.\n");
    }
    // 64K data buffer should be enough for the UDP packets.
    dataBufferLength = 1024 * 64;
    dataBuffer = malloc(dataBufferLength);
}

void reset_pixels_TS(void)
{
        for(int x=0; x<128; x++)
        {
            for(int y=0; y<128; y++)
            {
                pixelsTS[x][y]=0;
            }
        }
}

void get_frame(uint32_t nano_sec)
{
    cvSet(my_frame,cvScalarAll(0.5),0);
    cvSet(filt_img,cvScalarAll(0.5),0);

    uint32_t i;//just for loop - Ali
    uint32_t j,x_axis,y_axis;//just for the print loop - Ali
    CvScalar l_value = cvScalar(-0.05*delta, 0, 0, 0);
    CvScalar h_value = cvScalar(0.05*delta, 0, 0, 0);
    CvScalar p_value;
    uint32_t nSec=0;

    start = true;

    while (start)//nSec < nano_sec)
    {
        //clock_gettime(CLOCK_MONOTONIC, &start);
        ssize_t result = recv(listenUDPSocket, dataBuffer, dataBufferLength, 0);
        if (result <= 0)
        {
            printf("Error in recv() call: %d\n", errno);
            //break;
        }

        caerEventPacketHeader header = (caerEventPacketHeader) dataBuffer;

        uint32_t eventValid = caerEventPacketHeaderGetEventValid(header);
/*
		if (eventValid > 0) {
			void *firstEvent = caerGenericEventGetEvent(header, 0);
			void *lastEvent = caerGenericEventGetEvent(header, eventValid - 1);

			uint32_t firstTS = caerGenericEventGetTimestamp(firstEvent, header);
			uint32_t lastTS = caerGenericEventGetTimestamp(lastEvent, header);

			nSec += lastTS - firstTS;
		}
*/
        for(i=0; i<eventValid; i++) //loop - Ali
        {
            void *currentEvent = caerGenericEventGetEvent(header, i);
            j = (le32toh(*((uint32_t *) (((uint8_t *) currentEvent)))));
            j = j/2;//validity bit
            if(j&1)
            {
                p_value = h_value;
            }
            else
            {
                p_value = l_value;
            }
            j = j/32;// polarity bit and 4 NU bits total of 2^5
            x_axis = j&127;// 7 bits for x address
            j = j/8192;// 7 bits for x and 6 NU bits = 2^13 = 8192
            y_axis = j&127;// 7 bits for y axis

            uint32_t currentTS = caerGenericEventGetTimestamp(currentEvent, header);

            if(filter_pixels_TS(127-(int)x_axis, 127-(int)y_axis, currentTS))
            {
                update_pixel(filt_img, x_axis, y_axis, p_value);// cvSet2D(filt_img, 127-(int)x_axis, 127-(int)y_axis, p_value+cvScalar(0.5, 0, 0, 0));
            }
            update_pixel(my_frame, x_axis, y_axis, p_value);

            pixelsTS[127-(int)x_axis][127-(int)y_axis] = currentTS;
        }
    }
    //usleep(1);
}

void update_pixel(CvMat *image, uint32_t x, uint32_t y, CvScalar value)
{
    CvScalar temp = cvGet2D(image, 127-(int)x, 127-(int)y);
    temp.val[0] += value.val[0];

    cvSet2D(image, 127-(int)x, 127-(int)y, temp);
}


bool filter_pixels_TS(int x, int y, uint32_t ts)
{
    for(int i=x-neigh; i<x+neigh+1; i++) // search in a 3x3 neighborhood
    {
        for(int j=y-neigh; j<y+neigh+1; j++)
        {
            if( i>-1 && i<128 && j>-1 && j<128 ) //boundary for 0 <= x and y <= 127
            {
                if( !(i==x && j==y) )
                {
                    if( (ts - pixelsTS[i][j]) < delta_ts*((int) pow((double) 10,ts_scalar)) && pixelsTS[i][j]!=0x80000000 ) //
                        return true;
                }
            }
        }
    }
    return false;
}

void* lens_thread(void* param)
{
    while(key != 'q')
    {
        usleep(1);
        if(start)
        {
            lens_value = get_lens_value(lens_value);
            set_focus(fd, lens_value);
            start = false;
        }
    }
}

unsigned short get_lens_value(unsigned short current_lens_value)
{
    unsigned short new_lens_val = current_lens_value + l_m*LENS_STEP;

    if(new_lens_val==LENS_MAX)
    {
        l_m = -1;
    }

    if(new_lens_val==LENS_MIN)
    {
        l_m = 1;
    }
    return new_lens_val;

}

double pixelDistance(double u, double v)
{
    return sqrt(u*u + v*v);
}

double gaussianCoeff(double u, double v, double d0)
{
    double d = pixelDistance(u, v);
    return 1.0 - exp((-d*d) / (2*d0*d0));
}

void createGaussianHighPassFilter(CvSize my_size, double cutoffInPixels)
{
    ghpf = cvCreateMat(my_size.width, my_size.height, CV_32FC1);

    CvPoint center;
    center = cvPoint(my_size.width / 2, my_size.height / 2);

    int u, v;

    for(u = 0; u < my_size.height; u++)
    {
        for(v = 0; v < my_size.width; v++)
        {
            cvSet2D(ghpf, u, v, cvScalar(gaussianCoeff(u - center.y, v - center.x, cutoffInPixels),0,0,0));
        }
    }

    int cx = my_size.width/2;
    int cy = my_size.height/2;

    CvMat *q0, *q1, *q2, *q3;
    q0 = cvCreateMat(cx, cy, CV_32FC1);
    q1 = cvCreateMat(cx, cy, CV_32FC1);
    q2 = cvCreateMat(cx, cy, CV_32FC1);
    q3 = cvCreateMat(cx, cy, CV_32FC1);

    cvGetSubRect(ghpf, q0, cvRect(0, 0, cx, cy));
    cvGetSubRect(ghpf, q1, cvRect(cx, 0, cx, cy));
    cvGetSubRect(ghpf, q2, cvRect(0, cy, cx, cy));
    cvGetSubRect(ghpf, q3, cvRect(cx, cy, cx, cy));

    CvMat *tmp;                           // swap quadrants (Top-Left with Bottom-Right)
    tmp = cvCreateMat(cx, cy, CV_32FC1);
    cvCopy(q0, tmp, NULL);
    cvCopy(q3, q0, NULL);
    cvCopy(tmp, q3, NULL);

    cvCopy(q1, tmp, NULL);                  // swap quadrant (Top-Right with Bottom-Left)
    cvCopy(q2, q1, NULL);
    cvCopy(tmp, q2, NULL);

}

