#ifndef __VARIOPTICFOCUS_H__
#define __VARIOPTICFOCUS_H__
	
int lens_init(char *port);

int lens_close(int fd);

int set_focus(int fd, unsigned short val);

#endif  /* __VARIOPTICFOCUS_H__ */
